import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenegalEconomieComponent } from './senegal-economie.component';

describe('SenegalEconomieComponent', () => {
  let component: SenegalEconomieComponent;
  let fixture: ComponentFixture<SenegalEconomieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenegalEconomieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenegalEconomieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
