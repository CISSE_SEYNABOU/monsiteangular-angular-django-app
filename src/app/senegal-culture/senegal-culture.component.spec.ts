import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenegalCultureComponent } from './senegal-culture.component';

describe('SenegalCultureComponent', () => {
  let component: SenegalCultureComponent;
  let fixture: ComponentFixture<SenegalCultureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenegalCultureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenegalCultureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
