import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuAppSenegalComponent } from './menu-app-senegal/menu-app-senegal.component';
import { SenegalSliderComponent } from './senegal-slider/senegal-slider.component';
import { SenegalHistoireComponent } from './senegal-histoire/senegal-histoire.component';
import { SenegalGeographieComponent } from './senegal-geographie/senegal-geographie.component';
import { SenegalPolitiqueComponent } from './senegal-politique/senegal-politique.component';
import { SenegalEconomieComponent } from './senegal-economie/senegal-economie.component';
import { SenegalCultureComponent } from './senegal-culture/senegal-culture.component';
import { GreenSectionTitleDirective } from './green-section-title.directive';
import { AutoCodePipe } from './auto-code.pipe';
import { TodoListComponent } from './todo-list/todo-list.component';
import { ExportRegionsService} from './services/export-regions.service';
import { LoginComponent } from './login/login.component';
import {Routes} from '@angular/router';
import {RouterModule} from '@angular/router';
import { AuthentificationService } from './services/authentification.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AreactiveformComponent } from './areactiveform/areactiveform.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'Todolist', component: TodoListComponent},
  { path: 'Login', component: LoginComponent},
  { path: 'Application', component: AppComponent}
  

];

@NgModule({
  imports:[
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    MenuAppSenegalComponent,
    SenegalSliderComponent,
    SenegalHistoireComponent,
    SenegalGeographieComponent,
    SenegalPolitiqueComponent,
    SenegalEconomieComponent,
    SenegalCultureComponent,
    GreenSectionTitleDirective,
    AutoCodePipe,
    TodoListComponent,
    LoginComponent,
    AreactiveformComponent,
   


  ],
 
  
  providers: [ ExportRegionsService, AuthentificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
