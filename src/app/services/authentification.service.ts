import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  isAuth= false;

  signIn(){
    this.isAuth= true;
           
  }

  signOut(){
    this.isAuth= false;
  }

  constructor() { }
}
