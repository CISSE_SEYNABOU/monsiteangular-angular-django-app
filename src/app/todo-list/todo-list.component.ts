import { Component, OnInit } from '@angular/core';
import { NUMBER_TYPE } from '@angular/compiler/src/output/output_ast';
import { isNumber } from 'util';
import { stringify } from 'querystring';
import { listenToElementOutputs } from '@angular/core/src/view/element';
import { LoginComponent } from '../login/login.component';
import { AuthentificationService } from '../services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  title="Todo App";
  titre="Liste des TODO";
  public liste=[];
  public buttonName:any = 'See ToDo List';
  public show:boolean = false;
  public buttonNameRetour:any = 'See ToDo Form';
  public vide:boolean=true;
  public buttonNameSupp:any = 'Supprimer un todo';
  authstatut=this.authService.isAuth;
  
  ajouter () {
      this.vide=this.vide;
      // ENREGISTRER LE TODO DANS LE TABLEAU
      if($("#todoinput").val()){
        this.vide=!this.vide;
        let i=this.liste.length;
        this.liste[i]={id:i,nom:$("#todoinput").val()};
        var ligneAAfficher="<p style='background-color:white; border:1px solid;'>Todo ajouté avec succès!!!!</p>";
        $("#demo").append(ligneAAfficher);
        $("#bt").show();
        
        
      }
      else{
        
        var ligneAAfficher="<p style='background-color:red; border:1px solid;'>Entrée vide !!!!Réessayez</p>";
        $("#demoerror").append(ligneAAfficher);
      }
      
        
      
      
  }

  supprimer () {
    
    this.vide=this.vide;
    //SUPPRIMER LE TODO DU LE TABLEAU
    if($("#todoinput").val()){
      this.vide=!this.vide;
      let i=0;
      let j=this.liste.length;
      while (i<=j){
          if($("#todoinput").val(this.liste[i].nom)){
              this.liste.splice(i,1);
              var ligneAAfficher="<p style='background-color:white; border:1px solid;'>Todo supprimé avec succès!!!!</p>";
              $("#demo").append(ligneAAfficher);
              $("#bt").show();
             
          }
          
          i+=1;
          
      }
    }
    else{
        
      var ligneAAfficher="<p style='background-color:red; border:1px solid;'>Entrée vide !!!!Réessayez</p>";
      $("#demoerror").append(ligneAAfficher);
    }
  }
 

  
  

  
  
  toggle () { 
        this.show=!this.show;
        if(this.show){
      // afficher la liste des todo.
  
        $("#fieldsettodo").hide();
        $("#demo").hide();
        $("#bt").hide();
        $("#tabtodo").show();
      }
      
  }


  public todoform () {
      this.show =! this.show;
      this.vide=!this.vide;
        // afficher le formulaire des todo
        if (!this.show){
          
          $("#tabtodo").hide();
          $("#btretour").hide();
          $("#fieldsettodo").show();
          $("#bt").show();

        
        }
    }
        
        
  delog(){
      if($("#unlogin")){
        new LoginComponent(this.authService,this.router).onSignOut();
        this.router.navigate(['']);
      }
    }      
    

    constructor(private authService:AuthentificationService, private router:Router) { }

    ngOnInit() {
     
    }

   

}
