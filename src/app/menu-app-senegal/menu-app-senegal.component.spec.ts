import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuAppSenegalComponent } from './menu-app-senegal.component';

describe('MenuAppSenegalComponent', () => {
  let component: MenuAppSenegalComponent;
  let fixture: ComponentFixture<MenuAppSenegalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuAppSenegalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuAppSenegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
