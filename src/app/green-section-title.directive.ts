import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appGreenSectionTitle]'
})
export class GreenSectionTitleDirective {

  constructor(el: ElementRef, renderer: Renderer2) {
    renderer.setStyle(el.nativeElement, 'color', 'green');
    renderer.setStyle(el.nativeElement, 'font-size', '50px');
   }

}
