import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-areactiveform',
  templateUrl: './areactiveform.component.html',
  styleUrls: ['./areactiveform.component.css']
})
export class AreactiveformComponent implements OnInit {
  //sans validation de formulaire
  profileForm = new FormGroup ({
    prenom: new FormControl (''),
    nom: new FormControl (''),
    });
  myForm=this.myForm;  
  //avec validation de formulaire
  profileForm2 = this.fb.group({
    firstName: ['', Validators.required ],
    lastName: [''],
    address: this.fb.group({
    street: [''],
    city: [''],
    state : [''],
    zip: ['']
    }),
    });

  constructor(private fb:FormBuilder) { }
  
  
  ngOnInit() {
    

   
  }

}
