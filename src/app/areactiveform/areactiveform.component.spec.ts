import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreactiveformComponent } from './areactiveform.component';

describe('AreactiveformComponent', () => {
  let component: AreactiveformComponent;
  let fixture: ComponentFixture<AreactiveformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreactiveformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreactiveformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
