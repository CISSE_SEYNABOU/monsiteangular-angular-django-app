import { Component, OnInit } from '@angular/core';
import {User} from '../models/user.model';
import {AuthentificationService} from '../services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title="Login";
  public user=new User();
  authstatut:boolean;
  log(){
    this.user.username="root";
    this.user.password="passer";
    if($("#login")){
      if($("#username") && $("#password")){
        if (($("#username").val(this.user.username)) && ($("#password").val(this.user.password))){
          this.onSignIn();
          if(this.authstatut){
            $("#demo").append("connexion reussie");
          } 
          else
          $("#demo").append("connexion echouee");
            
        }
      }
    }
  }

  
  constructor(private authService:AuthentificationService, private router:Router) { }
  
  ngOnInit() {
    this.authstatut=this.authService.isAuth;
  }
  onSignIn(){
    this.authService.signIn();
    this.authstatut=this.authService.isAuth;
    this.router.navigate(['Todolist']);
     
    
  }

  onSignOut(){
    this.authService.signOut();
    this.authstatut=this.authService.isAuth;
  }
}
