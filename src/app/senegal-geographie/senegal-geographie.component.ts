import { Component, OnInit } from '@angular/core';
import { ExportRegionsService} from '../services/export-regions.service';
import {Region} from '../region';

@Component({
  selector: 'app-senegal-geographie',
  templateUrl: './senegal-geographie.component.html',
  providers: [ExportRegionsService],
  styleUrls: ['./senegal-geographie.component.css']
})


export class SenegalGeographieComponent implements OnInit {
title="Geographie"; 

regions:Region [];
getRegions(): void {
  this.regions = this.exportRegionsService.getRegions();
}

public show:boolean = false;
public buttonName:any = 'Voir les régions';
   toggle () {
    this.show =! this.show;
   
      // CHANGEZ LE NOM DU BOUTON.
      if (this.show)
        this.buttonName = "Cacher";
      else
        this.buttonName = "Afficher";
    }
  constructor(private exportRegionsService: ExportRegionsService) { }

  ngOnInit() {
    this.getRegions();
  }
  
}





  








